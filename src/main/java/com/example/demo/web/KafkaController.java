/**
 *  
 *  *All rights Reserved, Designed By wangkun
 *  *@projectName spring-kafka
 *  *@title     KafkaController
 *  *@version V1.0.0
 *  *@copyright 2020
 *  
 */
package com.example.demo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

/**
 *@Description TODO
 *@Author wangkun
 *@Date 2020/7/15 9:56
 *@Version 1.0
 */
@RestController
@RequestMapping("/api/kafka/")
public class KafkaController {

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;
    @GetMapping("send")
    @ResponseBody
    public boolean send(@RequestParam String message) {
        try {
            kafkaTemplate.send("test-topic", message);
            kafkaTemplate.send("test-topic2", message);
            System.out.println("消息发送成功...");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    @GetMapping("test")
    @ResponseBody
    public String test() {
        System.out.println("hello world!");
        return "ok";
    }
}
