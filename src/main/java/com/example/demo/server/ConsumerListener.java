/**
 *  
 *  *All rights Reserved, Designed By wangkun
 *  *@projectName spring-kafka
 *  *@title     ConsumerListener
 *  *@version V1.0.0
 *  *@copyright 2020
 *  
 */
package com.example.demo.server;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 *@Description TODO
 *@Author wangkun
 *@Date 2020/7/15 9:58
 *@Version 1.0
 */
@Component
public class ConsumerListener {

    @KafkaListener(topics = "test-topic")
    public void onMessage1(String message) {
        System.out.println("我是第一个消费者:" + message);
    }
    @KafkaListener(topics = "test-topic2")
    public void onMessage2(String message) {
        System.out.println("我是第二个消费者:" + message);
    }
}
